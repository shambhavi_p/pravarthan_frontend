import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { TimesheetsComponent } from './timesheets/timesheets.component';

import { FormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { TopmenuComponent } from './topmenu/topmenu.component';

import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

import { TimesheetService } from './services/timesheet.service';
import { ReportComponent } from './report/report.component';
import {ProjectService} from "./services/project.service";
import {ClientService} from "./services/client.service";
import {UserService} from "./services/user.service";
import {TimecellService} from "./services/timecell.service";
import {ReportService} from "./services/report.service";
import {CookieService} from 'angular2-cookie/services/cookies.service'
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import {AuthInterceptor} from './interceptor';


const appRoutes: Routes = [
  { path: 'timesheet', component: TimesheetsComponent },
  { path: 'report',      component: ReportComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    TimesheetsComponent,
    NavbarComponent,
    TopmenuComponent,
    ReportComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgbModule.forRoot()
  ],

  providers: [
    TimesheetService,
    ProjectService,
    ClientService,
    UserService,
    TimecellService,
    ReportService,
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]

})

export class AppModule { }
