import{ Component, OnInit }from '@angular/core';
import { Project } from "../models/project";
import { TimeRow } from "../models/time-row";
import { User } from "../models/user";
import { TimeCell } from "../models/time-cell";
import { TimesheetService } from '../services/timesheet.service';
import { ProjectService } from "../services/project.service";
import { TimecellService } from "../services/timecell.service";
import { UserService } from "../services/user.service";
import { FormBuilder, FormGroup, FormArray} from '@angular/forms';

@Component({
  selector: 'app-timesheets',
  templateUrl: './timesheets.component.html',
  styleUrls: ['./timesheets.component.css']
})

export class TimesheetsComponent implements OnInit {

  //controls visibility
  public TIMESHEET_CREATE_BUTTON_HIDDEN: boolean;
  public EDIT_BUTTON_HIDDEN: boolean;
  public SAVE_BUTTON_HIDDEN: boolean;
  public CANCEL_BUTTON_HIDDEN: boolean;
  public ADD_ROW_BUTTON_HIDDEN: boolean;

  // public myForm : FormGroup ;
  public projects: Project[];
  public time_rows: TimeRow[] = [];
  public selected_date = new Date();
  public time_cells: TimeCell[] = [];
  public user: User;

  constructor(private _timesheet_service: TimesheetService,
              private _project_service: ProjectService,
              private _timecell_service: TimecellService,
              private _user_service: UserService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.load_initial_data();
    this.set_initial_controls_visibility();
  }

  week_label = function() {
      return this._timecell_service.get_week_label_for_date(this.selected_date, this.selected_date);
  };


  build_dates_on_selected_week = function() {
    return this._timecell_service.get_weekly_time_cells_for_date(this.selected_date, this.selected_date);
  };


  get_row_total_hours(row: TimeRow) : number {
    return this._timecell_service.get_row_total_hours(row);
  };

  get_column_total_hours(param_cell: TimeCell) : number {
    return this._timecell_service.get_column_total_hours(this.time_rows, new Date(param_cell.date));
  };

  get_total_hours() : number {
    return this._timecell_service.get_total_hours(this.time_rows);
  };

  get_color_class(input_data: string) {
    return this._timesheet_service.get_color_class(input_data);
  };

  set_initial_controls_visibility(){
    this.TIMESHEET_CREATE_BUTTON_HIDDEN = true;
    this.EDIT_BUTTON_HIDDEN = true;
    this.SAVE_BUTTON_HIDDEN = true;
    this.CANCEL_BUTTON_HIDDEN = true;
    this.ADD_ROW_BUTTON_HIDDEN = true;
    console.log("set_controls" + this.get_total_hours())
    if(this.get_total_hours()==0){
      this.TIMESHEET_CREATE_BUTTON_HIDDEN = false;
    }else {
      this.EDIT_BUTTON_HIDDEN = false;
    }

  };

  load_initial_data(){
    
    this._user_service.get_current_user()
    .subscribe(data => {
    this.user = data;
    this._project_service.get_user_projects(this.user)
    .subscribe( data => {
    this.projects = data
    }, error => {
    console.log("User Not Authorised");});
    this.load_initial_timesheet();
    }, error => {
    console.log("Data Not loaded");});

    // this._project_service.get_user_projects(this.user)
    // .subscribe( data => {
    // this.projects = data
    // }, error => {
    // console.log("User Not Authorised");});
  };

  load_initial_timesheet() : void{
    let from_date: Date = this._timecell_service.get_initial_start_date(new Date());
    let to_date: Date = this._timecell_service.get_initial_end_date(new Date());
    this._timesheet_service.get_time_rows_for_dates(this.format_date(this.add_days(from_date,1)),
    this.format_date(this.add_days(to_date,1)), this.user.member_id.toString())
    .subscribe(data => {this.time_rows = data; this.set_initial_controls_visibility()},
    error => {console.log("Data Not loaded");});
  }


  change_project_selection(project_name: any, for_time_row: TimeRow) {

    let selected_project = this.projects.filter((item) => item.project_name == project_name)[0];
    for_time_row.project = selected_project;

  };

  click_create_new_timesheet(){

    this.time_rows = [{
      row_id:0,
      project:null,
      billable:true,
      user:this.user,
      client:null,
      description:'',
      time_cells: this.build_dates_on_selected_week(),
      is_in_edit_mode:true
    }];

    this.TIMESHEET_CREATE_BUTTON_HIDDEN = true;
    this.EDIT_BUTTON_HIDDEN = true;
    this.SAVE_BUTTON_HIDDEN = false;
    this.CANCEL_BUTTON_HIDDEN = false;
    this.ADD_ROW_BUTTON_HIDDEN = false;

  };

  click_cancel(){

    // this.load_initial_data();
    // this.set_initial_controls_visibility();
    this.click_select_coming_week();
    this.click_select_previous_week();
  };

  

  click_add_new_row(){
    let new_time_cells = this.build_dates_on_selected_week();
    this.time_rows.push({
      row_id: 0,
      project:null,
      billable:true,
      user:this.user,
      client:null,
      description:'',
      time_cells: new_time_cells,
      is_in_edit_mode:true
    });
  };

  click_edit_button(){
    this.TIMESHEET_CREATE_BUTTON_HIDDEN = true;
    this.EDIT_BUTTON_HIDDEN = true;
    this.SAVE_BUTTON_HIDDEN = false;
    this.CANCEL_BUTTON_HIDDEN = false;
    this.ADD_ROW_BUTTON_HIDDEN = false;

    this.time_rows.forEach(function (row) {
      row.is_in_edit_mode = true;
    });

  };

  click_remove_row(index: number, row: TimeRow){
    if(window.confirm("Are you sure?")){
      console.log("deleting");
    this.time_rows.splice(index, 1);
    if(row.row_id!=0){
      this._timesheet_service.delete_row(row.row_id)
      .subscribe(data=>{
        this._timesheet_service.
        get_time_rows_for_dates(row.time_cells[0].date,row.time_cells[6].date,this.user.member_id.toString())
        .subscribe(data=>this.time_rows = data);
      this.set_initial_controls_visibility()});
    }
  }
  };

  change_date_selection(evt: any){
    this.selected_date = new Date(evt.year,evt.month-1,evt.day);
    console.log(this.format_date(this.selected_date));
    let from_date:Date = this._timecell_service.get_initial_start_date(this.selected_date);
    let to_date: Date = this._timecell_service.get_initial_end_date(this.selected_date);
    this._timesheet_service.get_time_rows_for_dates(this.format_date(this.add_days(from_date,1)),
    this.format_date(this.add_days(to_date,1)), this.user.member_id.toString())
    .subscribe(data => {this.time_rows = data; this.set_initial_controls_visibility()},
    error => {console.log("Data Not loaded");});
  };

  click_select_previous_week() {
    this.selected_date = this._timesheet_service.get_previous_week_start_date(this.selected_date)
    let from_date: Date = this._timecell_service.get_initial_start_date(this.selected_date);
    let to_date: Date = this._timecell_service.get_initial_end_date(this.selected_date);
    this._timesheet_service.get_time_rows_for_dates(this.format_date(this.add_days(from_date,1)),
    this.format_date(this.add_days(to_date,1)), this.user.member_id.toString())
    .subscribe(data => {this.time_rows = data; this.set_initial_controls_visibility()},
    error => {console.log("Data Not loaded");});
  };

  click_select_coming_week() {
    this.selected_date = this._timesheet_service.get_coming_week_start_date(this.selected_date)
    let from_date: Date = this._timecell_service.get_initial_start_date(this.selected_date);
    let to_date: Date = this._timecell_service.get_initial_end_date(this.selected_date);
    this._timesheet_service.get_time_rows_for_dates(this.format_date(this.add_days(from_date,1)),
    this.format_date(this.add_days(to_date,1)), this.user.member_id.toString())
    .subscribe(data => {this.time_rows = data; this.set_initial_controls_visibility()},
    error => {console.log("Data Not loaded");});  
  };

  format_date(date1: Date) : string {
    let yyyy = date1.getFullYear();
    let mm = (date1.getMonth())>8?date1.getMonth()+1:"0"+(+date1.getMonth()+1);
    let dd = (date1.getDate().toString().length)>1?date1.getDate():"0"+(+date1.getDate());
    return yyyy + "-" + mm + "-" + dd;
  }

  add_days(date: Date, days: number): Date{
    date.setDate(date.getDate()+ days);
    return date;
  }

  click_save_button(){
    console.log(this.projects);
    let row_data = [];
    let is_put_data: Boolean = false;
    this.time_rows.forEach(function (time_row){
      let row = {};
      row["row_id"] = time_row.row_id;
      if(row["row_id"]!=0){
        is_put_data = true;
      }
      row["project_id"] = time_row.project.project_id;
      row["start_date"] = time_row.time_cells[0].date;
      row["end_date"] = time_row.time_cells[6].date;
      row["member_id"] = time_row.user.member_id;
      row["billable"] = time_row.billable;
      row["description"] = time_row.description;
      row["time cells"] = time_row.time_cells;
      row_data.push(row);
    });
    let rows = {};
    rows["data"] = row_data;
    if(is_put_data){
      this._timesheet_service.put_data_server(rows)
      .subscribe(data=>{
      this._timesheet_service.
      get_time_rows_for_dates(row_data[0]["start_date"],row_data[0]["end_date"],this.user.member_id.toString())
      .subscribe(data=>this.time_rows = data);
      this.set_initial_controls_visibility()});
    }
    else{
    this._timesheet_service.post_data_server(rows)
    .subscribe(data=>{
      this._timesheet_service.
      get_time_rows_for_dates(row_data[0]["start_date"],row_data[0]["end_date"],this.user.member_id.toString())
      .subscribe(data=>this.time_rows = data);
      this.set_initial_controls_visibility()});
  }
}

}
