import { Injectable } from '@angular/core';
import * as moment from "moment";
import {TimeRow} from "../models/time-row";
import {TimeCell} from "../models/time-cell";

@Injectable({
  providedIn: 'root'
})
export class TimecellService {

  constructor() { }

  get_week_label_for_date = function(from_date, to_date ){
    let first_part = moment(from_date).startOf('isoWeek').format('MMM DD');
    let second_part = moment(to_date).endOf('isoWeek').format('MMM DD');
    return first_part + ' - ' + second_part
  };

  get_initial_start_date = function(from_date) {
    let initial_start = moment(from_date).startOf('week').toDate();
    console.log("from: " + initial_start);
    return initial_start;
  };

  get_initial_end_date = function(to_date) {
    let initial_end = moment(to_date).endOf('week').toDate();
    return initial_end;
  };

  get_weekly_time_cells_for_date = function(from_date, to_date) {
    let temp_time_cell = []
    let startOfWeek = moment(from_date).startOf('isoWeek');
    let endOfWeek = moment(to_date).endOf('isoWeek');

    let days = [];
    let day = startOfWeek;

    while (day <= endOfWeek) {
      days.push(day.toDate());
      temp_time_cell.push ({
        date:moment(day.toDate()).format('YYYY-MM-DD'),
        cell_id:moment(day.toDate()).format('ddd') + ', '+ moment(day.toDate()).format('DD'),
        hours_spent:0.0
      });

      day = day.clone().add(1, 'd');
    }
    return temp_time_cell;
  };

  get_time_cell_for_dates = function(from_date, to_date) {
    let temp_time_cell = []
    let startDate = moment(from_date);
    let endDate = moment(to_date);

    let days = [];
    let day = startDate;

    while (day <= endDate) {
      days.push(day.toDate());
      temp_time_cell.push ({
        date:moment(day.toDate()).format('MMMM DD YYYY'),
        cell_id:moment(day.toDate()).format('ddd') + ', '+ moment(day.toDate()).format('D'),
        hours_spent:8.1
      });

      day = day.clone().add(1, 'd');
    }
    return temp_time_cell;
  };

  get_row_total_hours = function(time_row: TimeRow):number{
    let sum: number;
    sum = 0;
    time_row.time_cells.forEach(function (cell) {
      sum = sum + (+cell.hours_spent);
    });
    return +sum;
  };


  get_column_total_hours = function (time_rows: TimeRow[], date: Date):number {
    let column_sum: number;
    column_sum = 0;
    if (time_rows){
      time_rows.forEach(function (row) {
        row.time_cells.forEach(function (cell) {
          let date1 : Date = new Date(date);
          let date2 = new Date(cell.date);
          if (date1.getMonth()==date2.getMonth()
              && date1.getUTCDate()==date2.getUTCDate()
              && date1.getFullYear()==date2.getFullYear()){//.toDate().format('MMMM DD YYYY')){
            column_sum = column_sum + (+cell.hours_spent)
          }
        });
      });
    }
    return +column_sum;
  };

  
  get_total_hours = function(time_rows: TimeRow[]): number {
    let total: number;
    total = 0;
    if (time_rows){
      time_rows.forEach(function (row) {
        row.time_cells.forEach(function (cell) {
          total = total + (+cell.hours_spent)
        });
      });
    }
    return +total;
  };

}
