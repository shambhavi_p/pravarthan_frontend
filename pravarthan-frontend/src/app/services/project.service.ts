import { Injectable } from '@angular/core';
import {Project} from '../models/project';
import { User } from '../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookiesService } from '../services/cookies.service';
import { UserService } from '../services/user.service';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})

export class ProjectService {

  // public user: User;
  constructor(private http: HttpClient,
              private cookiesService: CookiesService,
              private userService: UserService) {

  }

  get_user_projects(user:User): Observable<Project[]> {
    console.table(user);
    let httpHeaders = new HttpHeaders().set("authToken", this.cookiesService.getCookie('authToken'));
    let user_id: string = user.work_email.split("@",1)[0];
    let url = "https://qa-internal.sahajsoft.com/api/user/" + user_id + "/projects";

    return <Observable<Project[]>> this.http.get( url, { headers: httpHeaders });

  }

  get_all_projects(): Observable<Project[]> {

    // let httpHeaders = new HttpHeaders().set("authToken", this.cookiesService.getCookie('authToken'));
    let url = environment.baseUrl + "projects/all/";

    return <Observable<Project[]>> this.http.get( url);//, { headers: httpHeaders });

  }
}
