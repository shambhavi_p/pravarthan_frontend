import { Injectable } from '@angular/core';
import {Client} from '../models/client';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookiesService } from '../services/cookies.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient, private cookiesService: CookiesService  ) { }

  get_clients(): Observable<Client[]> {

    // let httpHeaders = new HttpHeaders().set("authToken", this.cookiesService.getCookie('authToken'));
    let url = environment.baseUrl + "clients/all/";

    return <Observable<Client[]>> this.http.get(url);//, { headers: httpHeaders });

  }
}
