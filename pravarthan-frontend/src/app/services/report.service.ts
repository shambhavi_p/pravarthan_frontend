import { Injectable } from '@angular/core';
import {TimeRow} from "../models/time-row";
import { HttpClient, HttpParams } from '@angular/common/http';
import {TimecellService} from "./timecell.service";
import { Parser } from 'json2csv'
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
// import { start } from 'repl';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private _timecell_service: TimecellService,
              private http: HttpClient) { }

  get_report_for_dates(from_date, to_date): Observable<TimeRow[]> {
    let _url = environment.baseUrl + "?start_date=" + from_date + "&end_date=" + to_date;
    return <Observable<TimeRow[]>> this.http.get(_url);
  }

  get_report_for_query(params): Observable<TimeRow[]> {
    let _url = environment.baseUrl + "?";
    for(let key in params){
        _url = _url + key.toString() + "=" + params[key].toString() + "&";
    }
    return <Observable<TimeRow[]>> this.http.get(_url);
  }

  generate_report = function(time_rows, date_array) {

    //Constructing csv headers
    let fields = ['Project', 'Client', 'Member', 'Billable', 'Comments'];
    date_array.forEach(function (cell) {
      fields.push(cell);
    });
    fields.push("Total");

    //Constructing csv rows
    let report = [];
    time_rows.forEach(function (row) {
      let data = {};
      data["Project"] = row.project.project_name;
      data["Client"]=row.client.client_name;
      data["Member"]=row.user.display_name;
      data["Billable"]=row.billable;
      data["Comments"]=row.comments;
      row.time_cells.forEach(function (cell) {
        data[cell.date] = cell.hours_spent;
      });
      data["Total"]=new TimecellService().get_row_total_hours(row);
      report.push(data);

    });

    // Constructing csv summary row (last row)
    let summary_data = {};
    summary_data["Project"] = "Net work hours";
    summary_data["Client"]="";
    summary_data["Member"]="";
    summary_data["Billable"]="";
    summary_data["Comments"]="";
    date_array.forEach(function (cell) {
      summary_data[cell] = new TimecellService().get_column_total_hours(time_rows,cell);
    });
    summary_data["Total"]=new TimecellService().get_total_hours(time_rows);
    report.push(summary_data);

    const Json2csvParser = Parser;
    const json2csvParser = new Json2csvParser({ fields });
    const csv = json2csvParser.parse(report);
    return csv;
  }

}
