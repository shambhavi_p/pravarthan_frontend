import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {User} from "../models/user";
import { CookiesService } from '../services/cookies.service';
import { environment } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private cookiesService: CookiesService,
              private http: HttpClient) { }

  
  
  get_current_user(): Observable<User> {
    let url = environment.baseUrl + "user/current";
    return <Observable<User>> this.http.get( url);//, { headers: httpHeaders });
  }
  
  
  get_all_users(): Observable<User[]> {
    // let httpHeaders = new HttpHeaders().set("authToken", this.cookiesService.getCookie('authToken'));
    let url = environment.baseUrl + "users/all";
    return <Observable<User[]>> this.http.get(url);//, { headers: httpHeaders });
}

}
