import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from "moment";
import {TimeRow} from "../models/time-row";
import {TimecellService} from "./timecell.service";
import { CookiesService } from '../services/cookies.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService {

  constructor(private _timecell_service: TimecellService,
              private cookiesService: CookiesService,
              private http: HttpClient) { }


  get_color_class = function(input_data: string):string {
    if (input_data == "No" || input_data == "0"){
      return "non_edited"
    }
    else {
      return "edited"
    }

  };

  get_time_rows_for_dates(from_date, to_date, member_id): Observable<TimeRow[]> {
    console.log(member_id);
    let _url = environment.baseUrl +
               "?start_date=" + from_date + "&end_date=" + to_date +
                "&member_id=" + member_id;
    return <Observable<TimeRow[]>> this.http.get(_url);

  }

  get_previous_week_start_date = function(date) {
    return moment(date).subtract(1, 'weeks').startOf('isoWeek').toDate()
  };

  get_coming_week_start_date = function(date) {
    return moment(date).add(1, 'weeks').startOf('isoWeek').toDate()
  }

  get_billable_value = function(boolean_value) {
    return (boolean_value ? 'Yes' : 'No');
  }

  post_data_server(rows: any): Observable<any>{
  
    let url = environment.baseUrl + "post";
    return <Observable<any>>this.http.post(url, rows);//,{headers: httpHeaders});
  }

  put_data_server(rows: any): Observable<any>{
    let url=environment.baseUrl + "post";
    return <Observable<any>>this.http.put(url, rows);
  }

  delete_row(row_id: number): Observable<any>{
    let url=environment.baseUrl + "?row_id=" + row_id.toString();
    return <Observable<any>>this.http.delete(url);
  } 

}

//   let headers = new Headers({
  //     'Content-Type': 'application/json',
  //     'X-CSRFToken': this.cookiesService.getCookie('csrftoken')
  // });
  // let options = new RequestOptions({ headers: headers });
