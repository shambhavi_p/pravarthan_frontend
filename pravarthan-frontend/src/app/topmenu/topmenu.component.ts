import { Component, OnInit } from '@angular/core';
import { CookiesService } from '../services/cookies.service';

@Component({
  selector: 'app-topmenu',
  templateUrl: './topmenu.component.html',
  styleUrls: ['./topmenu.component.css']
})

export class TopmenuComponent implements OnInit {

  user_name :String;

  constructor(private cookiesService: CookiesService){
  }

  ngOnInit() {
    this.user_name = this.cookiesService.getCookie('user_name');
  }

  signOut() {
    
    document.cookie = "role=;domain=.sahajsoft.com;path=/";
    document.cookie = "authToken=;domain=.sahajsoft.com;path=/";
    document.cookie = "user_name=;domain=.sahajsoft.com;path=/";
    // this.cookiesService.setCookie('authToken','');
    // this.cookiesService.setCookie('user_name','');
    // this.cookiesService.setCookie('role','');
    window.location.href = 'https://qa-pravarthan.sahajsoft.com'
  }

}
