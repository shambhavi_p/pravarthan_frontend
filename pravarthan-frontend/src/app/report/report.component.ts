import { Component, OnInit, DoCheck } from '@angular/core';
import { Project } from "../models/project";
import { Client } from "../models/client";
import { User } from "../models/user";
import { ProjectService } from "../services/project.service";
import { ClientService } from "../services/client.service";
import { UserService } from "../services/user.service";
import { TimeRow } from "../models/time-row";
import { TimeCell } from "../models/time-cell";
import { TimecellService } from "../services/timecell.service";
import { ReportService } from "../services/report.service";
import { saveAs } from 'file-saver'
import * as moment from "moment";
// import { start } from 'repl';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  public projects: Project[] = [];
  public clients: Client[] = [];
  public users: User[] = [];
  public time_cells: TimeCell[] = [];
  public time_rows: TimeRow[] = [];
  public new_time_rows: TimeRow[] = [];
  public first_row: TimeRow;
  public date_array: Date[] = [];

  public selected_start_date: Date;
  public selected_end_date: Date;
  public selected_project: Project = null;
  public selected_user: User;
  public selected_client: Client;
  public selected_billability: boolean;

  constructor(private _project_service: ProjectService,
    private _client_service: ClientService,
    private _user_service: UserService,
    private _timecell_service: TimecellService,
    private _report_service: ReportService) {


  }

  ngOnInit() {
    this.selected_start_date = this._timecell_service.get_initial_start_date(new Date());
    this.selected_end_date = this._timecell_service.get_initial_end_date(new Date());
    let from_date: Date = this.selected_start_date;
    let to_date: Date = this.selected_end_date;
    this.date_array = this.get_date_array(this.selected_start_date, this.selected_end_date);
    this.selected_start_date = this._timecell_service.get_initial_start_date(new Date());
    this.selected_end_date = this._timecell_service.get_initial_end_date(new Date());

    this._project_service.get_all_projects()
      .subscribe(data => {
        this.projects = data;
        this._client_service.get_clients()
      .subscribe(data => {
        this.clients = data;
      }, error => {
        console.log("User Not Authorised");
      });
      this._user_service.get_all_users()
      .subscribe(data => { this.users = data },
        error => { console.log("User Not Authorised"); });
        this._report_service.get_report_for_dates(this.format_date(from_date), this.format_date(to_date))
        .subscribe((data) => { this.time_rows = data; this.change_time_rows() },
          error => { console.log("User Not Authorised"); });    
      }, error => {
        console.log("User Not Authorised");
      });
    
  }

  get_date_array(from_date: Date, end_date: Date): Date[] {
    let date: Date = from_date;
    let array: Date[] = [];
    for (; date <= end_date;) {
      array.push(new Date(date));
      date.setDate(date.getDate() + 1);
    }
    return array;
  }

  load_initial_data() {
    
  };

  change_time_rows() {
    this.new_time_rows = this.time_rows;
    let rows: TimeRow[] = this.time_rows;
    let i: number = 0;
    for (i = 0; i < rows.length; i++) {
      let row: TimeRow = rows[i];
      let x: TimeCell[] = this.get_time_cells(row);
      this.new_time_rows[i].time_cells = x;
    }
    this.first_row = this.new_time_rows[0];
  }

  get_time_cells(row: TimeRow): TimeCell[] {
    let time_cell_array: TimeCell[] = [];
    let given_time_cell_array: TimeCell[] = row.time_cells;
    let date: Date = new Date(this.selected_start_date);
    let i: number = 0;
    for (; date <= this.selected_end_date;) {
      if (i < given_time_cell_array.length &&
        this.is_date_equal(date, new Date(given_time_cell_array[i].date))) {
        time_cell_array.push(given_time_cell_array[i]);
        date.setDate(date.getDate() + 1);
        i++;
      }
      else {
        const x = <TimeCell>({
          cell_id: -1,
          hours_spent: 0.0,
          date: this.format_date(date),//.toString(),
        });
        time_cell_array.push(x);
        date.setDate(date.getDate() + 1);
      }
    }
    return time_cell_array;
  }

  is_date_equal(date1: Date, date2: Date): boolean {
    return (date1.getMonth() == date2.getMonth()
      && date1.getDay() == date2.getDay()
      && date1.getFullYear() == date2.getFullYear());
  }


  get_row_total_hours(row: TimeRow): number {
    return this._timecell_service.get_row_total_hours(row);
  };

  get_column_total_hours(date: Date): number {
    return this._timecell_service.get_column_total_hours(this.time_rows, date);
  };

  get_total_hours(): number {
    return this._timecell_service.get_total_hours(this.time_rows);
  };

  click_view_button() {
    let params = {};
    let start_date = this.format_date(this.selected_start_date);
    let end_date = this.format_date(this.selected_end_date);
    params["start_date"] = start_date;
    params["end_date"] = end_date;

    if (this.selected_project && this.selected_project.project_id) {
      params["project_id"] = this.selected_project.project_id;
    }

    if (this.selected_client && this.selected_client.id) {
      params["client_id"] = this.selected_client.id;
    }

    if (this.selected_user && this.selected_user.member_id) {
      params["member_id"] = this.selected_user.member_id;
    }

    if (this.selected_billability) {
      params["billable"] = this.selected_billability;
    }

    this.date_array = this.get_date_array(new Date(start_date), new Date(end_date));

    this._report_service.get_report_for_query(params).
      subscribe(data => { this.time_rows = data; this.change_time_rows(); },
        error => { console.log("User Not Authorised"); });
  }

  output_project(project_id: any): void {
    this.selected_project = this.projects.find(project => project.project_id === project_id);
  }


  onCurrentStartDateSelect(evt: any) {
    this.selected_start_date = new Date(evt.year, evt.month - 1, evt.day);
  }

  onCurrentEndDateSelect(evt: any) {
    this.selected_end_date = new Date(evt.year, evt.month - 1, evt.day);
  }

  format_date(date1: Date): string {
    let yyyy = date1.getFullYear();
    let mm = (date1.getMonth().toString().length) > 1 ? date1.getMonth() : "0" + (+date1.getMonth() + 1);
    let dd = (date1.getDate().toString().length) > 1 ? date1.getDate() : "0" + (+date1.getDate());
    return yyyy + "-" + mm + "-" + dd;
  }


  click_export_button() {
    let formatted_date_array = [];
    this.date_array.forEach(function (date1){
      let yyyy = date1.getFullYear();
      let mm = (date1.getMonth().toString().length) > 1 ? date1.getMonth() : "0" + (+date1.getMonth() + 1);
      let dd = (date1.getDate().toString().length) > 1 ? date1.getDate() : "0" + (+date1.getDate());
      formatted_date_array.push(yyyy + "-" + mm + "-" + dd);
    });
      
    let csv = this._report_service.generate_report(this.time_rows, formatted_date_array);
    let blob = new Blob([csv], { type: 'text/csv;charset=utf-8' });
    saveAs(blob, "report_timesheet (" + new Date().toDateString() + ").csv");
  }

}
