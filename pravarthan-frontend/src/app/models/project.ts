export interface Project {
  project_name: string;
  client_name: string;
  project_id: number;
}
