export interface User {
  display_name: string;
  member_id: number;
  work_email: string;
}
