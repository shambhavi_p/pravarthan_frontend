export interface TimeCell {
  date: string;
  cell_id: number;
  hours_spent: number;
}
