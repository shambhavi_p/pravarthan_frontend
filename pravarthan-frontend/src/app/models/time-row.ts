import {Project} from "./project";
import {TimeCell} from "./time-cell";
import {User} from "./user";
import {Client} from "./client";

export interface TimeRow {
  row_id:number;
  user:User;
  client:Client;
  project: Project;
  billable: boolean;
  description: string;
  time_cells: TimeCell[];
  is_in_edit_mode: boolean;
}
