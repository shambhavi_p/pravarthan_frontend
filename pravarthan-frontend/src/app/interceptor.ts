import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor,HttpResponse, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookiesService } from './services/cookies.service'
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor{

    constructor(private cookieService: CookiesService){

    }
    intercept(request: HttpRequest<any>, next:HttpHandler): Observable<HttpEvent<any>>{
        const modified = request.clone({
            setHeaders: {'authToken': this.cookieService.getCookie('authToken')}
        });
        return next.handle(modified)
        .pipe(
	        tap(event => {
	          if (event instanceof HttpResponse) {
	             
	            console.log(event.status);
	          }
	        }, error => {
                document.cookie = "role=;domain=.sahajsoft.com;path=/";
                document.cookie = "authToken=;domain=.sahajsoft.com;path=/";
                document.cookie = "user_name=;domain=.sahajsoft.com;path=/";
                window.location.href = "https://qa-pravarthan.sahajsoft.com";

	        })
	      )

    };
    }


